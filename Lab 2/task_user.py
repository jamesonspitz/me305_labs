''' @File         task_user.py
    @brief        User interface for Console and user communication.
    @details      Ask user to specify whether they want to zero motor position, 
                  get motor position, get motor delta, collect data, or end data collection. 
                  Based off user input, a shared state variable is set for task encoder to run the correct action.
    @author       Jameson Spitz
    @author       Derick Louie
    @date         October 21, 2021
    @copyright    CC BY
'''

class Task_user():
    
    ''' @brief      User task.
        @details    Reads user inputs and stores in state variables.
    '''
    
    def __init__(self, state, user, reset, runs):
        
        ''' @brief              Constructs the user task.
            @details            Defines variables and initializes user task
            @param state        A shares.Share object for the FSM state 
            @param user         A shares.Share object for user input
            @param reset        A shares.Share object used to reset timers
            @param runs         A shares.Share object used to print on the first run of the FSM
        '''
        ## @brief     Shared state variable between task user and task encoder.
        #  @details   The state is specified here in task user and used in task encoder to run the action the user specified.

        self.my_state = state
        
        ## @brief     Shared state variable between task user and task encoder.
        #  @details   Action signals to return to state zero. Action is zeroed out in task encoder once a task has been performed.
        
        self.action = user
        
        ## @brief     Controls the timer in task encoder.
        #  @details   Signals task encoder to reset starttimes each time state zero is ran.
        
        self.reset = reset
        
        ## @brief     run counter for task user
        #  @details   When run is zero for state 'g', the data collection print statement is ran. If run is not equal to zero, state 'g' runs but does not continually print.
        
        self.runs = runs
        
    def run(self):
        
        ''' @brief              Runs one iteration of FSM
            @details            Reads user inputs and performs actions such as printing to console and writes to the state variable.
        '''
        
        if(self.action.read() == 0):
            self.action.write(input('z: Zero the position of encoder 1 \np: Print out the position of encoder 1 \nd: Print out the delta for encoder 1 \ng: Collect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list \ns: End data collection prematurely \n'))
            self.reset.write(1)
        
        if(self.action.read() == 'z'):
            self.my_state.write('z')
            print(self.my_state.read())
            
        elif(self.action.read() == 'p'):
            self.my_state.write('p')
            
        elif(self.action.read() == 'd'):
            self.my_state.write('d')
            
        elif(self.action.read() == 'g'):
            self.my_state.write('g')
            #print(self.runs.read())
            if(self.runs.read() == 0):
                print('Data collection started. Press s to terminate.')
            self.runs.write(self.runs.read() + 1)

                
        else:                           # Sends user task back to prompt user if no if statement has been entered.
            print('Invalid input.') 
            self.action.write(0)
            self.runs.write(0)          # Set run counter back to zero due to invalid run.
            

