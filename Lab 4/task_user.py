''' @file         task_user.py
    @brief        User interface for Console and user communication.
    @details      Ask user to specify whether they want to zero motor position, 
                  get motor position, get motor delta, collect data, or end data collection. 
                  Based off user input, a shared state variable is set for task encoder to run the correct action.
    @author       Jameson Spitz
    @author       Derick Louie
    @date         November 17, 2021
    @copyright    CC BY
'''

class Task_user():
    
    ''' @brief      User task.
        @details    Reads user inputs and stores in state variables.
    '''
    
    def __init__(self, state, user, reset, runs, enc, duty, faulted, Kp, target):
        
        ''' @brief              Constructs the user task.
            @details            Defines variables and initializes user task
            @param state        A shares.Share object for the FSM state 
            @param user         A shares.Share object for user input
            @param reset        A shares.Share object used to reset timers
            @param runs         A shares.Share object used to print on the first run of the FSM
            @param enc          A shares.Share object used to specify which motor to perform tasks on
            @param duty         A shares.Share object used to set duty cycle for each motor
            @param faulted      A shares.Share object used to flag if motor has been faulted
        '''
        ## @brief     Shared state variable between task user and task encoder.
        #  @details   The state is specified here in task user and used in task encoder to run the action the user specified.

        self.my_state = state
        
        ## @brief     Shared state variable between task user and task encoder.
        #  @details   Action signals to return to state zero. Action is zeroed out in task encoder once a task has been performed.
        
        self.action = user
        
        ## @brief     Controls the timer in task encoder.
        #  @details   Signals task encoder to reset starttimes each time state zero is ran.
        
        self.reset = reset
        
        ## @brief     run counter for task user
        #  @details   When run is zero for state 'g', the data collection print statement is ran. If run is not equal to zero, state 'g' runs but does not continually print.
        
        self.runs = runs
        
        ## @brief      Specifies encoder 1 or 2
        #  @details    Tasks are performed on specific encoder depending on the number of enc
        
        self.enc = enc
        
        ## @brief      Specifies how fast to run the motor at
        #  @details    Duty is a percentage of how much power a user wants to run the motor at
        
        self.duty = duty
        
        ## @brief    Flag for when motor is faulted
        #  @details  Is true when motor has faulted and prevents motor from being re-enabled. Is false once user clears fault condition.
        #  
        
        self.faulted = faulted
        
        ## @brief    Kp constant
        #  @details  Constant that error is multipled by in P controller
        #
        
        self.Kp = Kp
        
        ## @brief    Target value
        #  @details  Target velocity for P controller
        #
        
        self.target = target
        
    def run(self):
        
        ''' @brief              Runs one iteration of FSM
            @details            Reads user inputs and performs actions such as printing to console and writes to the state variable.
        '''
        
        
        if(self.action.read() == 0):
            print('-----------------------------------------------------------------------------')
            print('z: Zero the position of encoder 1. \nZ: Zero the position of encoder 2. \np: Print out the position of encoder 1 \nP: Print out the position of encoder 2. \nd: Print out the delta for encoder 1 \nD: Print out the delta for encoder 2. \nE or e: Enable motor driver. \nN or n: Disable motor driver.\nm: Enter a duty cycle for motor 1. \nM: Enter a duty cycle for motor 2. \nC or c: Clear fault condition triggered by the DRV8847. \ng: Collect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list \nG: Print encoder data for encoder 2 in the same fasion as encoder 1. \nS or s: End data collection prematurely')
            print('-----------------------------------------------------------------------------')
            self.action.write(input('Command: '))
            self.reset.write(1)
            
            if(self.action.read() == 'z'):
                self.my_state.write('z')
                self.enc.write(1)
                
            elif(self.action.read() == 'Z'):
                self.my_state.write('Z')
                self.enc.write(2)
                
            elif(self.action.read() == 'p'):
                self.my_state.write('p')
                self.enc.write(1)
                
            elif(self.action.read() == 'P'):
                self.my_state.write('P')
                self.enc.write(2)
                
            elif(self.action.read() == 'd'):
                self.my_state.write('d')
                self.enc.write(1)
                
            elif(self.action.read() == 'D'):
                self.my_state.write('D')
                self.enc.write(2)
                
            elif(self.action.read().lower() == 'e'):
                if(self.faulted.read() == True):
                    print('Fault condition must be cleared before motor can move.')
                    self.action.write(0)
                else:
                    self.my_state.write('e')
                
            elif(self.action.read() == 'm'):
                self.enc.write(1)
                if(self.faulted.read() == True):
                    print('Fault condition must be cleared before motor can move.')
                self.my_state.write('m')
                self.duty.write(int(input('Please enter a duty cycle (0 - 100) for Encoder 1: ')))
                
                
            elif(self.action.read() == 'M'):
                self.enc.write(2)
                if(self.faulted.read() == True):
                    print('Fault condition must be cleared before motor can move.')
                self.my_state.write('M')
                self.duty.write(int(input('Please enter a duty cycle (0 - 100) for Encoder 2: ')))
                
                
            elif(self.action.read() == 'g'):
                self.enc.write(1)
                if(self.faulted.read() == True):
                    print('Fault condition must be cleared before motor can move.')
                    self.action.write(0)
                else:
                    if(self.runs.read() == 0):
                        print('Data collection started. Press s or S to terminate.')
                    self.my_state.write('g')
                    self.runs.write(self.runs.read() + 1)
                
            elif(self.action.read() == 'G'):
                self.enc.write(2)
                if(self.faulted.read() == True):
                    print('Fault condition must be cleared before motor can move.')
                    self.action.write(0)
                else:
                    if(self.runs.read() == 0):
                        print('Data collection started. Press s or S to terminate.')
                    self.my_state.write('G')
                    self.runs.write(self.runs.read() + 1)
            
            elif(self.action.read().lower() == 'n'):
                self.my_state.write('n')
                      
            elif(self.action.read().lower() == 'c'):
                self.my_state.write('c')
                
            elif(self.action.read() == '1'):
                if(self.faulted.read() == True):
                    print('Fault condition must be cleared before motor can move.')
                    self.action.write(0)
                else:
                    self.my_state.write('1')
                    self.Kp.write(input('Enter Kp value: '))
                    self.target.write(input('Enter target velocity: '))
                if(self.runs.read() == 0):
                        print('Data collection started. Press s or S to terminate.')
                
            else:                           # Sends user task back to prompt user if no if statement has been entered.
                print('Invalid input.') 
                self.action.write(0)
                self.runs.write(0)          # Set run counter back to zero due to invalid run.
            

