''' @file       panel_driver.py
    @brief      Panel Driver initializes board pins to get x and y position of a touch on a panel and flag if anything is touching the panel.
    @details    Initializes the board pins as high/low and out_pp/in to read ADC values of touch placement on the panel and convert the analog ADC location to a digital mm location. Can read the x posiion, y position, and z test to determine if anything is touching the plate.
    @author     Jameson Spitz
    @author     Derick Louie
    @date       December 8th, 2021
    @copyright  CC BY
'''
from pyb import Pin
from pyb import ADC
import time
from time import sleep as wait

class Panel_driver:
    
    ''' @brief              Can test touch on a panel and returns touch x and y location in mm.
        @details            Converts ADC readings to digital [mm] with calibration coefficient knowing that max ADC is 4095.
    '''
    
    def __init__(self, pin_ym, pin_xm, pin_yp, pin_xp, size_x, size_y, center_x, center_y ):
        
        ''' @brief              Initializes panel pins and panel dimensions.
            @details            Associates x and y positive and x and y minus terminals to specified board pins. 
            @param              The respective board pin associated with the y negative terminal.
            @param              The respective board pin associated with the x negative terminal.
            @param              The respective board pin associated with the y positive terminal.
            @param              The respective board pin associated with the x positive terminal.
            @param              The width of the board in mm, associated with the x coordinaate reading of the panel.
            @param              The height of the board in mm, associated with the y coordinaate reading of the panel.
            @param              The x coordinate in mm at the center of the panel.
            @param              The y coordinate in mm at the center of the panel.
        '''
        
        ## @brief      Negative y terminal pin.
        #  @details    Assigned to corresponding board pin.
        
        self.pin_ym = pin_ym
        
        ## @brief      Negative x terminal pin.
        #  @details    Assigned to corresponding board pin.
        
        self.pin_xm = pin_xm
        
        ## @brief      Positive y terminal pin.
        #  @details    Assigned to corresponding board pin.
        
        self.pin_yp = pin_yp
        
        ## @brief      Positive x terminal pin.
        #  @details    Assigned to corresponding board pin.
        
        self.pin_xp = pin_xp
        
        ## @brief      Width of board
        #  @details    Assigned as length of board in x direction [mm]
        
        self.size_x = size_x
        
        ## @brief      Height of board
        #  @details    Assigned as length of board in y direction [mm]
        
        self.size_y = size_y
        
        ## @brief      x measurement at center of board
        #  @details    mm reading for x at center of board
        
        self.center_x = center_x
        
        ## @brief      y measurement at center of board
        #  @details    mm reading for y at center of board
        
        self.center_y = center_y
        
    def scan_xyz(self):
        
        ''' @brief              Calculates x and y positions, and checks if there is touch in the board
            @details            Resets configuration of pins for each position scan. Pins that are read from ADC are floated (set as IN) while the pins being scanned for are set as OUT_PP.
            @return             Returns tuple consisting af x and y localtions in mm, and flag if board is being touched.
        '''
        
        ## @brief      Changes mode of pin_ym
        #  @details    Sets pin_ym to OUT_PP to later set it as low
        
        pin_ym = Pin(self.pin_ym, Pin.OUT_PP)
        
        ## @brief      Changes mode of pin_yp
        #  @details    Sets pin_ym to OUT_PP to later set it as high
        
        pin_yp = Pin(self.pin_yp, Pin.OUT_PP)
        
        ## @brief      Changes mode of pin_xm
        #  @details    Sets pin_ym to IN so ADC can read it
        
        pin_xm = Pin(self.pin_xm, Pin.IN)
        
        ## @brief      Changes mode of pin_xp
        #  @details    Sets pin_ym to IN so ADC can read it
        
        pin_xp = Pin(self.pin_xp, Pin.IN)
        
        pin_ym.low()                   # Sets ym as low and yp as high to induce current from yp to ym
        pin_yp.high()
        
        wait(.000004)                  # Waits to avoid settling time from energizing pins
        
        ## @brief      Instantiates ADC object
        #  @details    Creates analog object from pin_xm
        
        adc_xm = ADC(pin_xm)

        ## @brief      Reads ADC value
        #  @details    Gets analog value for y posotion on panel

        analog_y = adc_xm.read()
        
        ## @brief      Analog to digital conversion.
        #  @details    Converts analog value of y reading to millimeters using a ratio of current ADC to max ADC and then accounts for center bias.
        
        scaled_y = analog_y*self.size_y/4095-self.center_y
        
        # X position scan
        
        pin_ym = Pin(self.pin_ym, Pin.IN)                        # Alters pin modes to read for x position
        pin_yp = Pin(self.pin_yp, Pin.IN)
        pin_xm = Pin(self.pin_xm, Pin.OUT_PP)
        pin_xp = Pin(self.pin_xp, Pin.OUT_PP)    
        
        pin_xm.low()                   # Sets xm as low and xp as high to induce current from xp to xm
        pin_xp.high()
        
        wait(.000004)                  # Waits to avoid settling time from energizing pins
        
        ## @brief      Instantiates ADC object
        #  @details    Creates analog object from pin_ym
        
        adc_ym = ADC(pin_ym)
        
        ## @brief      Reads ADC value
        #  @details    Gets analog value for x posotion on panel
        
        analog_x = adc_ym.read()
        
        ## @brief      Analog to digital conversion.
        #  @details    Converts analog value of x reading to millimeters using a ratio of current ADC to max ADC and then accounts for center bias.
        
        scaled_x = analog_x*self.size_x/4095-self.center_x
    
        # Z scan to determine if there is pressure on plate.    
    
        pin_ym = Pin(self.pin_ym, Pin.IN)                        # Alters pin modes to read for z scan
        pin_yp = Pin(self.pin_yp, Pin.OUT_PP)
        pin_xm = Pin(self.pin_xm, Pin.OUT_PP)
        pin_xp = Pin(self.pin_xp, Pin.IN) 
        
        pin_xm.low()                    # Sets xm as low and yp as high to induce current from yp to xm
        pin_yp.high()
        
        wait(.000004)                   # Waits to avoid settling time from energizing pins
        
        ## @brief      Instantiates ADC object
        #  @details    Creates analog object from pin_ym
        
        adc_ym = ADC(pin_ym)
        
        if(adc_ym.read() < 4000 ):
            
            ## @brief      Flag for panel touch
            #  @details    If ADC reading is < 4000, then there is panel touch and scan_z is true
            
            scan_z = True
        else:
            scan_z = False
        
        return scaled_x, scaled_y, scan_z
        
    
        
        
if __name__ == '__main__':
   
    i=0
    my_touchpanel = Panel_driver(Pin.board.PA0, Pin.board.PA1, Pin.board.PA6, Pin.board.PA7, 176, 100, 88, 50)
    starttime=time.ticks_us()
    while (True):
        #print('X:',my_touchpanel.scan_x(), 'Y:',my_touchpanel.scan_y(), 'Z:',my_touchpanel.scan_z())
        print(my_touchpanel.scan_xyz())
    print(time.ticks_us()-starttime)
    
        