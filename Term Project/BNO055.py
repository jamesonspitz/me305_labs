''' @file       BNO055.py
    @brief      IMU driver to collect data and perform other functions on BNO055
    @details    IMU driver provides functions to change operating mode, calibration, and collecting angle and velocity values in degrees
    @author     Jameson Spitz
    @author     Derick Louie
    @date       November 10, 2021
    @copyright  CC BY
'''

from pyb import I2C
import struct
import utime

class BNO055:
    
    ''' @brief              Uses I2C to get IMU data
        @details            Gets IMU euler angles, angular velocities, and reads/writes calibration data
    '''
    
    def __init__(self):
        
        ''' @brief              Sets up i2c and defines variables
            @details            Sets up i2c in master mode and create buffer variables to store data
        '''
        
        ## @brief    I2c variable 
        #  @details  Sets i2c object in master mode
        #
        self.i2c = I2C(1, I2C.MASTER)
        
        ## @brief     Radius buffer
        #  @details   2 byte buffer to store radius values
        #
        self.buf_radius = bytearray(2)
        
        ## @brief     Buffer
        #  @details   6 byte buffer to store angle and velocity values
        #
        self.buf = bytearray(6)
        
        ## @brief     Calibration values
        #  @details   8 byte bytearray to store calibration status
        #
        self.cal_bytes = bytearray(8)
        
        ## @brief     Calibration coefficient buffer
        #  @details   24 byte bytearray to store calibration coefficients
        #
        self.calib_coeff_buf= bytearray(24)
        
        ## @brief     Calibration coefficients
        #  @details   Stores final calibration coefficients
        #
        self.calib_coeff = 0

    def operating_mode(self, mode):
        ''' @brief              Sets operating mode
            @details            Sets operating mode of sensor by writing to register 0x3D
        '''
        self.i2c.mem_write(mode, 0x28, 0x3D)     #writes mode to 0x3D, the operating mode register
        
    def set_units(self, units):
        ''' @brief              Sets units
            @details            Sets units of sensor by writing to register 0x3B
        '''
        self.i2c.mem_write(units, 0x28, 0x3B)    #writes units to 0x3B, the units register

    def calib_status(self):
        ''' @brief              Gets calibration status of sensor and prints it
            @details            Reads calibration status and stores in 8 byte bytearray
            @return             Returns calibrated boolean
        '''
        self.i2c.mem_read(self.cal_bytes, 0x28, 0x35)
        
        ## @brief     Calibration status
        #  @details   Calibration status of sensor, 3 is fully calibrated
        #
        
        cal_status = ( self.cal_bytes[0] & 0b11,
              (self.cal_bytes[0] & 0b11 << 2) >> 2,
              (self.cal_bytes[0] & 0b11 << 4) >> 4,
              (self.cal_bytes[0] & 0b11 << 6) >> 6)     #bit shifting to read data

        if(cal_status[0] == 3 and cal_status[1] == 3 and cal_status[2] == 3 and cal_status[3] == 3):
        #sets calibrated to true if all values are 3
            
        ## @brief     Calibrated flag
        #  @details   Variable set to true when sensor is calibrated and false when sensor is not calibrated
        #
            calibrated = True
        else:
            calibrated = False
           # print('Waiting for sensor calibration') 
           # print("Calibration Status:", cal_status)
        return calibrated
        
    def euler_angle(self, calibrated):
        ''' @brief              Gets euler angles
            @details            Gets euler angles by reading 0x1A and storing in 6 byte bytearray to be unpacked into a tuple and printed          
        '''
        #if(calibrated):
        self.i2c.mem_read(self.buf, 0x28, 0x1A)     #reads 0x1A, the euler angle registers
        
        ## @brief     Angle signed ints
        #  @details   Unpacked angle values from buffer
        #
        angle_signed_ints = struct.unpack('<hhh', self.buf)
        
        ## @brief     Angle values
        #  @details   Angle values in a tuple containing heading, roll, and pitch
        #
        angle_vals = tuple(angle_int/16 for angle_int in angle_signed_ints)
        
        #print('Angles: (Heading, Roll, Pitch)',angle_vals)
        return angle_vals
        
    def euler_velocity(self, calibrated):
        ''' @brief              Gets euler velocities
            @details            Gets euler velocities by reading 0x14 and storing in 6 byte bytearray to be unpacked into a tuple and printed            
        '''
        if(calibrated):
            self.i2c.mem_read(self.buf, 0x28, 0x14)
            
            ## @brief     Velocity signed ints
            #  @details   Unpacked velocity values from buffer
            #
            vel_signed_ints = struct.unpack('<hhh', self.buf)   #unpacks buffer
            
            ## @brief     Velocity values
            #  @details   Velocity values in a tuple, containing X, Y, and Z
            #
            vel_vals = tuple(vel_int/16 for vel_int in vel_signed_ints)
            
            #print('Velocity: (X, Y, Z)',vel_vals)
            return vel_vals
            
    def calib_read(self):
        ''' @brief              Reads and stores calibration coefficients
            @details            Reads calibration coefficients and
            @param              sensor specifies which sensor to read calibration coefficients for
            @return             Returns calibration coefficients of the specified sensor
        '''
        runs = 0
        if(calibrated == False):
            print('Waiting for calibration')
        elif(calibrated == True):
            runs = 1
            self.i2c.mem_write(0, 0x28, 0x3D)

            self.i2c.mem_read(self.calib_coeff_buf, 0x28, 0x55)
                
            self.calib_coeff = struct.unpack('BBBBBBBBBBBBBBBBBBBBBB', self.calib_coeff_buf)
            
            print(self.calib_coeff_buf)
            print(list(self.calib_coeff))
            hex_list = [hex(int(x)) for x in list(self.calib_coeff)]
            #print('hex', hex_list)
            print(type(self.calib_coeff))
    
            textfile = open("IMU_cal_coeffs.txt", "w")
            for element in hex_list:
                if (runs >= 1):
                    runs += 1
                    if(runs <= 22):
                        textfile.write(element + ',')
                    else:
                        textfile.write(element)
                    #print('write')
            
            textfile.close()

        return self.calib_coeff

    
    def calib_write(self):
        ''' @brief              Writes calibration coefficients
            @details            Writes calibration coefficients stored in 'IMU_cal_coeffs.txt' file 
        '''
        
        self.i2c.mem_write(0, 0x28, 0x3D)
        textfile = open('IMU_cal_coeffs.txt', 'r')
        textfile = str(textfile.read())
        coeffs = textfile.split(",")
        coeffs = tuple(coeffs)
        coeffs = [int(x) for x in list(coeffs)]

        coeffs_byte = struct.pack('BBBBBBBBBBBBBBBBBBBBBB', *coeffs)
        
        self.i2c.mem_write(coeffs_byte, 0x28, 0x55)
        
        print('Calibration coefficients written')


        
        
if __name__ == '__main__':
    BNO055().operating_mode(12) #sets operating mode to NDOF

    
    while True:
        calibrated = BNO055().calib_status()
        print('Angles: (Heading, Roll, Pitch)',BNO055().euler_angle(calibrated))
        print('Velocity: (X, Y, Z)',BNO055().euler_velocity(calibrated))
        #BNO055().calib_read()
        BNO055().calib_write()
        print('\n')
        BNO055().operating_mode(12)
        utime.sleep(0.5)
        #print('done')
        #utime.sleep(5)