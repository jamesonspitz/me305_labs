''' @file        task_encoder.py
    @brief       Interacts with encoder driver to perform encoder actions.
    @details     Zeros out motor position, gets position, gets delta, collects data for 30 seconds, and interrupts data collection.
    @author      Jameson Spitz
    @author      Derick Louie
    @date        Novermber 17, 2021
    @copyright   CC BY
'''
import time
import encoder
import pyb
import math
import gc

class Task_encoder():
    
    ''' @brief      Encoder task.
        @details    Implements finite state machine for encoder actions.
    '''
    
    def __init__(self, state, user, reset, runs, enc, duty):
        
        ''' @brief              Constructs an encoder task.
            @details            Defines variables and initializes encoder task
            @param state        A shares.Share object for the FSM state 
            @param user         A shares.Share object for user input
            @param reset        A shares.Share object used to reset timers
            @param runs         A shares.Share object used to print on the first run of the FSM
            @param enc          A shares.Share object used to specify which motor to perform tasks on
            @param duty         A shares.Share object used to set duty cycle for each motor
        '''
        
        ## @brief      Instantiates connection between keyboard and Necleo.
        #  @details    Used to interrupt data collection when 's' is pressed.
        #
        
        self.ser = pyb.USB_VCP()
        gc.enable()                 # Sets up a garbage collecter to allow for larger data storage
        
        ## @brief      Instantiates encoder 1 object.
        #  @details    Passes through important pins, timer number, and encoder type
        #
        
        self.my_encoder = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4, pyb.Timer.ENC_A)
        
        ## @brief      Instantiates encoder 2 object.
        #  @details    Passes through important pins, timer number, and encoder type
        #
        
        self.my_encoder2 = encoder.Encoder(pyb.Pin.board.PC6, pyb.Pin.board.PC7, 8, pyb.Timer.ENC_A)
        
        ## @brief      Shared variable to specify which state task encoder runs
        #  @details    Is written by user in task user file
        #
        
        self.my_state = state
        
        ## @brief      Reset flag to re-initialize certain parameters
        #  @details    Used to reset timers after data collection has been finished or terminated
        #
        
        self.reset = reset
        
        ## @brief      List of position data in ticks
        #  @details    Stores motor position for specified time or until collection is terminated.
        #
        
        self.datacollection=[]
        
        ## @brief      List of time data in seconds
        #  @details    Stores time for specified duration or until collection is terminated.
        #
        
        self.time_record = []
        
        ## @brief      List of position data in radians
        #  @details    Stores motor position for specified time or until collection is terminated.
        #
        
        self.position_record = []
        
        ## @brief      List of delta (velocity) data.
        #  @details    Stores motor velocity for specified time or until collection is terminated.
        #
        
        self.delta_record = []
        
        ## @brief      Stores the old position of motor to track delta.
        #  @details    Used to find delta over a given amount of time in task encoder.
        #
        
        self.old_position = 0
        
        ## @brief    Time elapsed variable.
        #  @details  Variable used to store time elapsed since timer reset.
        #

        self.timeelapsed=0
        
        ## @brief    Start time variable.
        #  @details  Variable used to store start time for first timer for time elapsed calculation.
        #
        
        self.starttime=time.ticks_ms()
        
        ## @brief    Start time counter variable.
        #  @details  Variable used to store start time for second timer to collect data at a certain interval.
        #

        self.starttimec=time.ticks_ms()
        
        ## @brief      Writes back to task user to specify state.
        #  @details    Writes back to initial prompting state after a task in encoder is performed.
        #
        
        self.action = user
        
        ## @brief      Counts the number of runs in task encoder.
        #  @details    Performs some printing and initializing tasks on the first run.
        #
        
        self.runs = runs
        
        ## @brief      Specifies encoder 1 or 2
        #  @details    Tasks are performed on specific encoder depending on the number of enc
        #
        
        self.enc = enc
        
        ## @brief      Specifies how fast to run the motor at
        #  @details    Duty is a percentage of how much power a user wants to run the motor at
        #
        
        self.duty = duty
    
    def run(self):

        ''' @brief              Runs one iteration of FSM
            @details            Performs encoder actions based on state variable
        '''
        gc.collect()                # Begins collecting extra data
        
        if(self.reset.read() == 1):
            self.starttime=time.ticks_ms()
            self.starttimec=time.ticks_ms()
            self.timeelapsed=0
            self.timeelapsedc=0
            self.reset.write(0)
        
        if(self.ser.any()):
            char = self.ser.read(1).decode()
            if(char.lower() == 's'):
                self.action.write(0)
                self.runs.write(0)
                
                if(self.enc.read() == 1):
                    print('Encoder 1 Position Data', self.datacollection)
                
                elif(self.enc.read() == 2):   
                    print('Encoder 2 Position Data', self.datacollection)
 
                self.datacollection=[]
                self.time_record = []
                self.position_record = []
                self.delta_record = []
  
        self.my_encoder.update()
        self.my_encoder2.update()
        
        if(self.my_state.read().lower() == 'z'):
            if(self.enc.read() == 1):
                self.my_encoder.set_position(0)
                print('Encoder 1 Position Reset')
                
            elif(self.enc.read() == 2):
                self.my_encoder2.set_position(0)
                print('Encoder 2 Position Reset')
            self.action.write(0)
            
        if(self.my_state.read().lower() == 'p'):
            if(self.enc.read() == 1):
                print('Encoder 1 Current Position:',self.my_encoder.get_position())
                
            elif(self.enc.read() == 2):    
                print('Encoder 2 Current Position:',self.my_encoder2.get_position())
            self.action.write(0)
            
        if(self.my_state.read().lower() == 'd'):
            if(self.enc.read() == 1):
                print('Encoder 1 Delta:', self.my_encoder.get_delta())
            elif(self.enc.read() == 2):
                print('Encoder 2 Delta:', self.my_encoder2.get_delta())
            self.action.write(0)
            
        if(self.my_state.read().lower() == 'g'):
            
            if(self.timeelapsed < 30000):
                
                if(self.runs.read() == 1):
                    print('---------------------Initiallized------------------------------')
                    if(self.enc.read() == 1):
                        self.old_position = self.my_encoder.get_position()
                    elif(self.enc.read() == 2):
                        self.old_position = self.my_encoder2.get_position()
                    
                self.timeelapsed=time.ticks_ms()-self.starttime
                
                self.timeelapsedc=time.ticks_ms()-self.starttimec
                
                self.my_encoder.update()
                
                self.my_encoder2.update()
            
                if(self.enc.read() == 1):
                    if(self.timeelapsedc >= 100):
                        self.datacollection.append(self.my_encoder.get_position())
                        self.starttimec=time.ticks_ms()
                        
                        self.time_record.append(self.timeelapsed/1000)
                        self.position_record.append(self.my_encoder.get_position()*20*math.pi/20000)
                        self.delta_record.append((self.my_encoder.get_position()- self.old_position)*20*math.pi/20000/(self.timeelapsedc/1000))
                        self.old_position = self.my_encoder.get_position()
                        
                        
                elif(self.enc.read() == 2):
                    if(self.timeelapsedc >= 100):
                        self.datacollection.append(self.my_encoder2.get_position())
                        self.starttimec=time.ticks_ms()
                        
                        self.time_record.append(self.timeelapsed/1000)
                        self.position_record.append(self.my_encoder.get_position()*20*math.pi/10000)
                        self.delta_record.append((self.my_encoder.get_position()- self.old_position)*20*math.pi/10000/(self.timeelapsedc/1000))
                        self.old_position = self.my_encoder.get_position()
                  
                    
                self.runs.write(self.runs.read() + 1)
                
            else:
                if(self.enc.read() == 1):
                    print('Encoder 1 Time data [s]\n')
                    print(self.time_record, '\n')
                    print('Encoder 1 Position data [rad]\n')
                    print(self.position_record, '\n')
                    print('Encoder 1 Delta data [rad/s]\n')
                    print(self.delta_record)
                
                elif(self.enc.read() == 2):   
                    print('Encoder 2 Time data [s]\n')
                    print(self.time_record, '\n')
                    print('Encoder 2 Position data [rad]\n')
                    print(self.position_record, '\n')
                    print('Encoder 2 Delta data [rad/s]\n')
                    print(self.delta_record)
                    
                self.datacollection=[]
                self.time_record = []
                self.position_record = []
                self.delta_record = []
                self.action.write(0)
                self.runs.write(0)
            
        
        